import {services} from 'Src/services/services';
import {appConfig} from 'Src/constant';

export function get(params) {
    return services.get({...params, API_HOST: appConfig.API_HOST + 'suites'});
}

export function getList(params: any = {}) {
    return services.getList({...params, API_HOST: appConfig.API_HOST + 'suites'});
}

export function create(params) {
    return services.create({...params, API_HOST: appConfig.API_HOST + 'suites'});
}

export function update(params) {
    return services.update({...params, API_HOST: appConfig.API_HOST + 'suites'});
}

export function del(params) {
    return services.del({...params, API_HOST: appConfig.API_HOST + 'suites'});
}