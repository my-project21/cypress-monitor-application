import React from 'react';

const DashBoard = React.lazy(() => import('Src/modules/Dashboard'));

export default [
    {
        state: 'dashboard',
        path: '/dashboard',
        exact: true,
        component: DashBoard,
        resources: [
            
        ]
    }
];