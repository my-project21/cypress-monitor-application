// Libraries
import React, {useEffect, useState} from 'react';
import {Card, Col, Row} from 'antd';

// Services
import * as cypressTestService from 'Services/CypressTest/index'

// Styles
import {FormStyled} from './styled';

type TRun = {
    _id: string,
    title: string,
    failCount?: number,
    pendingCount?: number,
    skippedCount?: number,
    startDate?: string,
    successCount?: number,
    suites?: Array<{}>
}

const DashBoard: React.FC = () => {
    const [runs, setRuns] = useState<TRun[]>([]);

    useEffect(() => {
        getCypressTests();
    }, []);

    const getCypressTests = () => {
        cypressTestService.getList()
            .then((res) => {
                if (res && res.data && res.data.data) {
                    const {data} = res.data;
    
                    setRuns(data);  
                }
            })
            .catch(() => {
                //
            });
       
    };

    return (
        <FormStyled>
            <Row gutter={[10, 10]}>
                {
                    runs.length ? (
                        runs.map(run => {
                                return (
                                    <Col key={run._id}>
                                        <Card>{run.title}</Card>
                                    </Col>
                                )
                        })
                    ) : null
                }
            </Row>
        </FormStyled>
    );
};

export default DashBoard;
