import styled from 'styled-components';

export const DefaultMainStyled = styled.div`
    position: relative;
    background-color: #f0f2f5;
    padding: 20px;
    height: calc(100vh - 60px);
`;