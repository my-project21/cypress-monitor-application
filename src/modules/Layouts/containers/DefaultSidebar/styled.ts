import styled from 'styled-components';

export const DefaultSidebarStyled = styled.nav`
    float: left;
    position: fixed;
    width: 100%;
    height: 100vh;
    box-shadow: 0px 0px 5px #bfbfbf;
`;