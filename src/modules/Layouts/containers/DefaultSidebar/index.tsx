// Libraries
import React from 'react';

// Styled
import {DefaultSidebarStyled} from './styled';

function DefaultSidebar() {
    return (
        <DefaultSidebarStyled>
            Side bar
        </DefaultSidebarStyled>
    );
}

export default DefaultSidebar;
