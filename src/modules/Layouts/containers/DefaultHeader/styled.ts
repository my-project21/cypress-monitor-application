import styled from 'styled-components';

export const DefaultHeaderStyled = styled.nav`
    position: sticky;
    display: flex;
    align-items: center;
    padding: 0px 20px;
    top: 0;
    width: 100%;
    height: 60px;
`;