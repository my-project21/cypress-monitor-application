// Libraries
import React from 'react';

// Styled
import {DefaultHeaderStyled} from './styled';

function DefaultHeader() {
    return (
        <DefaultHeaderStyled>
            <i className='logo-antalyser'  />
        </DefaultHeaderStyled>
    );
}

export default DefaultHeader;
